import * as firebase from "firebase/app";
import "firebase/analytics";
import "firebase/firestore";

import { Chart, ChartDataSets, ChartPoint } from "chart.js";
import "chartjs-adapter-date-fns";

import { firebaseConfig, chartjsConfig } from "./config";
import {
  getCanvasElementByID,
  distributeColors,
  get2dContextFromCanvas,
} from "./utils";

window.onload = () => {
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  // Draw initial chart
  const ctx = get2dContextFromCanvas(getCanvasElementByID("myChart"));
  const chart: Chart = new Chart(ctx, chartjsConfig);

  // Update datasets in chart config
  const startDate = new Date(Date.now() - 24 * 60 * 60 * 1000);
  const places = new Map<string, { dateTime: Date; value: number }[]>();
  firebase
    .firestore()
    .collection("pm_2_5")
    .where("time", ">=", firebase.firestore.Timestamp.fromDate(startDate))
    .orderBy("time")
    .get()
    .then((snapshot) => {
      snapshot.docs.forEach((doc) => {
        // Get value
        const place = doc.get("place_name");
        const data = {
          dateTime: doc.get("time").toDate(),
          value: doc.get("value"),
        };

        // Add to map
        if (places.has(place)) {
          const oldValues = places.get(place);
          if (!oldValues) {
            throw new Error(`panic: places should have this "${place}" key`);
          }
          places.set(place, [...oldValues, data]);
        } else {
          places.set(place, [data]);
        }
      });

      // Generate line colors for each place
      const colors = distributeColors(places.size);

      // Set dataset to new data
      chartjsConfig.data = {
        datasets: [...places.entries()].map(
          (place, index): ChartDataSets => ({
            label: place[0],
            fill: colors[index],
            borderColor: colors[index],
            data: place[1].map(
              (point): ChartPoint => ({
                x: point.dateTime,
                y: point.value,
              })
            ),
          })
        ),
      };

      // Update chart
      chart.update();
    });
};
