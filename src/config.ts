import { ChartConfiguration } from "chart.js";

// Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyANOv2wKootB9N-uu6-BwHAAhcE83c3oUM",
  authDomain: "my-line-notify.firebaseapp.com",
  databaseURL: "https://my-line-notify.firebaseio.com",
  projectId: "my-line-notify",
  storageBucket: "my-line-notify.appspot.com",
  messagingSenderId: "1063814790222",
  appId: "1:1063814790222:web:c9e059927a044208ee571f",
  measurementId: "G-622NRKMC57",
};

// Chart config
export const chartjsConfig: ChartConfiguration = {
  type: "line",
  data: {},
  options: {
    responsive: true,
    // aspectRatio: 0.5,
    maintainAspectRatio: false,
    title: {
      display: true,
      text: "PM2.5 Time Point Data",
    },
    scales: {
      xAxes: [
        {
          type: "time",
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Time",
          },
          time: { minUnit: "hour" },
          ticks: {
            major: {
              fontStyle: "bold",
              fontColor: "#FF0000",
            },
          },
        },
      ],
      yAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: "PM2.5",
          },
        },
      ],
    },
  },
};
