export const getCanvasElementByID = (id: string): HTMLCanvasElement => {
  const element = document.getElementById(id);

  if (!(element instanceof HTMLCanvasElement)) {
    throw new Error(`The element of ID ${id} is not a HTMLCanvasElement.`);
  }

  return element;
};

export const get2dContextFromCanvas = (canvasElement: HTMLCanvasElement) => {
    const ctx = canvasElement.getContext("2d")
    if (!ctx) {
        throw new Error(`Cannot get 2d context from this canvas`)
    }

    return ctx
}

// distributeColors generate an array of distributed color for an easy seperation
export function distributeColors(amount: number): string[] {
    const colors: string[] = []
    for (var i = 0; i < amount; i++) {
      colors.push(`hsl(${Math.round(360 * i / amount)}, 90%, 65%)`)
    }
  
    return colors
  }
  