import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from '@rollup/plugin-commonjs'
import analyze from "rollup-plugin-analyzer";
import { terser } from "rollup-plugin-terser";
import typescript from "@rollup/plugin-typescript";

export default {
  input: "src/index.ts",
  output: {
    file: "./build/bundle.js",
    format: "iife",
    sourcemap: true,
  },
  plugins: [
    nodeResolve({ browser: true }),
    typescript(),
    commonjs(),
    terser({ format: { comments: false } }),
    analyze({ limit: 7, summaryOnly: true }),
  ],
};
